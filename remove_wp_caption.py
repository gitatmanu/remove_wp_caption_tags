import re, os

# Format content
contents = open("input.txt", "r").read()
contents = re.sub(r'\[/?caption[^\]]*?\]', '', contents)

# Output
output = open("output.txt", "w")
output.write(contents)
output.close()
